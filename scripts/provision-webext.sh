#!/usr/bin/env bash
#
# Install dependencies and compile things on a fresh system for the Web
# Extension.
#
# Tested on Debian Bullseye.
#

# Parameters
DIRNAME="`dirname $0`"
DEPENDENCIES_DEBIAN="npm python2"
DEPENDENCIES_NPM="vue-cli@4.5.15 web-ext@6.6.0"
NODE_VERSION="14.18.2"

# Debian
sudo apt-get update
sudo apt-get install -y $DEPENDENCIES_DEBIAN

# NPM
sudo npm install -g n
/usr/local/bin/n $NODE_VERSION
sudo /usr/local/bin/n $NODE_VERSION
sudo npm install -g $DEPENDENCIES_NPM
